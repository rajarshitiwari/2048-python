
Implementations of the 2048 game
================================

The famous 2048 game is implemented in fortran and python. I started the c code,
but due to lack of enough skills and time, never managed to finish it.

+ The fortran code is primarily used to study the analytics of the game.
+ The python version provides a way to play it inside a terminal, as its ncurses based.

Please send your comments to - rajarshi84@gmail.com
