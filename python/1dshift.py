#!/usr/bin/python

#from os import system
import random
import numpy

def shift_increase(arr):
	#SHIFT ALL ZEROS TO RIGHT
	c = 0; num = len(arr)
	i = num-1;
	while ( i > 0 ):
		if (arr[i] == 0):
			c = 0
			for j in range(i,-1,-1):
				if (arr[j] == 0):
					c += 1
				else:
					break
				#
			#
			if (c != 0):
				for j in range(i,c-1,-1):
					arr[j] = arr[j-c]
				#
				arr[0:c] = 0
			#
		#
		i -= 1
	#
	#add consecutive numbers
	c = 0
	i = num-1
	score = 0;
	while ( i >= 1 ):
		if (arr[i] == 0):
			break
		#
		if (arr[i] == arr[i-1]):
			term = arr[i] * 2
			arr[i] = term
			score += term
			for j in range(i-1,0,-1):
				arr[j] = arr[j-1]
			#
			arr[0] = 0
		#
		i -= 1
	#
	return score
#

def shift_decrease(arr):
	#shift all zeros to right
	c = 0; num = len(arr)
	i = 0
	while ( i < num - 1 ):
		if (arr[i] == 0):
			c = 0
			for j in range(i,num):
				if (arr[j] == 0):
					c += 1
				else:
					break
				#
			#
			if (c != 0):
				for j in range(i,num-c,1):#???
					arr[j] = arr[j+c]
				#
				arr[num-c:] = 0
			#
		#
		i += 1
	#
	#add consecutive numbers
	c = 0
	i = 0
	score = 0;
	while ( i < num-1 ):
		if (arr[i] == 0):
			break
		#
		if (arr[i] == arr[i+1]):
			term = arr[i] * 2
			arr[i] = term
			score += term
			for j in range(i+1,num-1,1):
				arr[j] = arr[j+1]
			#
			arr[num-1] = 0
		#
		i += 1;
	#
	return score
#


print 'increase'
#arr = [2,0,0,2,0,2,0,4,0,4,0,0,8]
arr = [2,4,0,4,0,0,0,0,0,4,2,2,8]
arr = numpy.array(arr)
print arr
score = shift_increase(arr)
print arr


print 'decrease'
#arr = [2,0,0,2,0,2,0,4,0,4,0,0,8]
arr = [2,4,0,4,0,0,0,0,0,4,2,2,8]
arr = numpy.array(arr)
print arr
score = shift_decrease(arr)
print arr

