PROGRAM MAIN
  IMPLICIT NONE

  INTEGER, PARAMETER :: LSIZE = 10
  INTEGER, DIMENSION(LSIZE) :: ARRAY
  INTEGER :: I,J,C

  ARRAY = [0,2,2,4,0,2,0,4,4,0]

  ARRAY = [2,0,0,2,0,0,0,4,8,0]

  PRINT'(10(I2))',ARRAY
  !CALL SHIFT_INCREASE(ARRAY)
  CALL SHIFT_DECREASE(ARRAY)
  PRINT'(10(I2))',ARRAY

CONTAINS

  SUBROUTINE SHIFT_INCREASE(ARR)
    IMPLICIT NONE
    INTEGER, DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER :: NUM, I, J, C
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, 1, -1
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, 1+C, -1
                ARR(J) = ARR(J-C)
             END DO
             ARR(1:C) = 0
          END IF
          !PRINT*,I,C
          !PRINT'(10(I2))',ARR
       END IF
       I = I - 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I-1))THEN
          ARR(I) = ARR(I) * 2
          DO J = I-1, 2, -1
             ARR(J) = ARR(J-1)
          END DO
          ARR(1) = 0
       END IF
       I = I - 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_INCREASE







  SUBROUTINE SHIFT_DECREASE(ARR)
    IMPLICIT NONE
    INTEGER, DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER :: NUM, I, J, C
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, NUM
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, NUM-C
                ARR(J) = ARR(J+C)
             END DO
             ARR(NUM-C+1:NUM) = 0
          END IF
          PRINT*,I,C
          PRINT'(10(I2))',ARR
       END IF
       I = I + 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I+1))THEN
          ARR(I) = ARR(I) * 2
          DO J = I+1, NUM - 1
             ARR(J) = ARR(J+1)
          END DO
          ARR(NUM) = 0
       END IF
       I = I + 1
    END DO
    PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_DECREASE


  SUBROUTINE SWAP(A,B)
    IMPLICIT NONE
    INTEGER, INTENT(INOUT) :: A,B
    INTEGER :: X
    X = B
    B = A
    A = X
    RETURN
  END SUBROUTINE SWAP

END PROGRAM MAIN
