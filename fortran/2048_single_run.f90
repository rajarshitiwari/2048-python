PROGRAM MAIN
  IMPLICIT NONE

  INTEGER(8), PARAMETER :: LSIZE = 8, NSIZE = LSIZE**2
  INTEGER(8), DIMENSION(LSIZE,LSIZE) :: ARRAY,PREV_ARR
  INTEGER(8) :: I1,I2,S1,S2,ITEST,ISAMPLE,NUM_SAMPLE
  INTEGER(8) :: GAME_COUNT, GAME_SCORE
  INTEGER(8) :: AVG_COUNT, AVG_SCORE
  REAL(8) :: T1,T2
  CHARACTER(LEN=10) :: STR


  NUM_SAMPLE = 100

  CALL RANDOM_SEED()
  ARRAY = 0
  CALL RANDOM_INTEGER(1,LSIZE,S1)
  CALL RANDOM_INTEGER(1,LSIZE,S2)
  ARRAY(S1,S2) = 2

  !CALL WRITE_ARRAY(ARRAY,6)
  GAME_COUNT = 0
  GAME_SCORE = 0
  DO
     PREV_ARR = ARRAY
     CALL RANDOM_INTEGER4(ITEST)
     SELECT CASE(ITEST)
     CASE(1) !1
        !! SHIFT RIGHT !!
        DO I1 = 1, LSIZE
           CALL SHIFT_INCREASE(ARRAY(I1,:),GAME_SCORE)
        END DO
     CASE(2) !2
        !! SHIFT LEFT !!
        DO I1 = 1, LSIZE
           CALL SHIFT_DECREASE(ARRAY(I1,:),GAME_SCORE)
        END DO
     CASE(3) !3
        !! SHIFT DOWN !!
        DO I2 = 1, LSIZE
           CALL SHIFT_INCREASE(ARRAY(:,I2),GAME_SCORE)
        END DO
     CASE(4) !4
        !! SHIFT UP !!
        DO I2 = 1, LSIZE
           CALL SHIFT_DECREASE(ARRAY(:,I2),GAME_SCORE)
        END DO
     CASE DEFAULT
        PRINT*,'SOMETHING WRONG'
     END SELECT

     IF (ANY(ARRAY /= PREV_ARR)) THEN
        GAME_COUNT = GAME_COUNT + 1
     ELSE IF (ANY(ARRAY == 0)) THEN
        !PRINT*,'CAN NOT MOVE!'
        CYCLE
     ELSE
        !PRINT*,'GAME_COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
        !WRITE(1,*)GAME_COUNT,GAME_SCORE
        !WRITE(1,*)
        !STOP 'GAME OVER'
        EXIT! LOOP_SAMPLE
     END IF

     DO WHILE (ANY(ARRAY == 0)) 
        CALL RANDOM_INTEGER(1,LSIZE,S1)
        CALL RANDOM_INTEGER(1,LSIZE,S2)
        IF (ARRAY(S1,S2)==0)THEN
           CALL RANDOM_INTEGER4(ITEST)
           SELECT CASE(ITEST)
           CASE(1)
              ARRAY(S1,S2) = 2
           CASE(2,3,4)
              ARRAY(S1,S2) = 2
           CASE DEFAULT
              PRINT*,'SOMETHING WORNG!!'
           END SELECT
           EXIT
        ELSE
           CYCLE
        END IF
     END DO

     CALL NUMTOSTR(GAME_COUNT,STR)
     !OPEN(1,FILE='state.'//TRIM(STR)//'.dat')
     !PRINT*,'GAME_COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
     !WRITE(1,*)GAME_COUNT,GAME_SCORE

     !CALL WRITE_ARRAY(ARRAY,6)
     !CALL SYSTEM('sleep 0.5')
     !PAUSE
     PRINT*,'GAME COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
  END DO
  PRINT*,'GAME COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
  OPEN(1,FILE='state.dat')
  CALL WRITE_ARRAY_C(ARRAY,1)
  CLOSE(1)

CONTAINS

  SUBROUTINE SHIFT_INCREASE(ARR,SCORE)
    IMPLICIT NONE
    INTEGER(8), DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER(8), INTENT(INOUT) :: SCORE
    INTEGER(8) :: NUM, I, J, C, TERM
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, 1, -1
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, 1+C, -1
                ARR(J) = ARR(J-C)
             END DO
             ARR(1:C) = 0
          END IF
          !PRINT*,I,C
          !PRINT'(10(I2))',ARR
       END IF
       I = I - 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I-1))THEN
          TERM = ARR(I) * 2
          ARR(I) = TERM
          SCORE = SCORE + TERM
          DO J = I-1, 2, -1
             ARR(J) = ARR(J-1)
          END DO
          ARR(1) = 0
       END IF
       I = I - 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_INCREASE

  SUBROUTINE SHIFT_DECREASE(ARR,SCORE)
    IMPLICIT NONE
    INTEGER(8), DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER(8), INTENT(INOUT) :: SCORE
    INTEGER(8) :: NUM, I, J, C, TERM
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, NUM
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, NUM-C
                ARR(J) = ARR(J+C)
             END DO
             ARR(NUM-C+1:NUM) = 0
          END IF
          !PRINT*,I,C
          !PRINT'(10(I2))',ARR
       END IF
       I = I + 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I+1))THEN
          TERM = ARR(I) * 2
          ARR(I) = TERM
          SCORE = SCORE + TERM
          DO J = I+1, NUM - 1
             ARR(J) = ARR(J+1)
          END DO
          ARR(NUM) = 0
       END IF
       I = I + 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_DECREASE

  SUBROUTINE RANDOM_INTEGER(N1,N2,IRAN)
    INTEGER(8), INTENT(IN) :: N1,N2
    INTEGER(8), INTENT(OUT) :: IRAN
    REAL(8) :: X
    CALL RANDOM_NUMBER(X)
    IRAN = N1 + FLOOR(X*DBLE(N2 + 1 - N1))
    RETURN
  END SUBROUTINE RANDOM_INTEGER

  SUBROUTINE RANDOM_INTEGER4(IRAN)
    INTEGER(8), INTENT(OUT) :: IRAN
    REAL(8) :: X
    CALL RANDOM_NUMBER(X)
    IRAN = 1 + INT(4.0D0 * X)
    RETURN
  END SUBROUTINE RANDOM_INTEGER4

  SUBROUTINE WRITE_ARRAY(ARRAY,UNIT)
    IMPLICIT NONE
    INTEGER(8), DIMENSION(:,:), INTENT(IN) :: ARRAY
    INTEGER(8), INTENT(IN) :: UNIT
    INTEGER(8) :: I1,I2,NUM
    NUM = SIZE(ARRAY,DIM=1)
    DO I1 = 1, NUM
       DO I2 = 1, NUM
          WRITE(UNIT,'(ES23.10,1X)',ADVANCE='NO')DBLE(ARRAY(I1,I2))
       END DO;WRITE(UNIT,*)
    END DO
    RETURN
  END SUBROUTINE WRITE_ARRAY

  SUBROUTINE WRITE_ARRAY_C(ARRAY,UNIT)
    IMPLICIT NONE
    INTEGER(8), DIMENSION(:,:), INTENT(IN) :: ARRAY
    INTEGER(8), INTENT(IN) :: UNIT
    INTEGER(8) :: I1,I2,NUM
    NUM = SIZE(ARRAY,DIM=1)
    DO I1 = 1, NUM
       DO I2 = 1, NUM
          WRITE(UNIT,'(I2,1X,I2,1X,ES23.10,1X)')I1,I2,DBLE(ARRAY(I1,I2))
       END DO;WRITE(UNIT,*)
    END DO
    RETURN
  END SUBROUTINE WRITE_ARRAY_C

  SUBROUTINE NUMTOSTR(NUM,STR)
    INTEGER(8), INTENT(IN) :: NUM
    CHARACTER(LEN=*), INTENT(OUT) :: STR
    INTEGER(8) :: I1,I2,N
    N = NUM
    I2 = N
    STR = ''
    IF(N == 0)THEN
       STR = CHAR(48)
       RETURN
    END IF
    DO WHILE(I2 > 0)
       I1 = MOD(N,10)
       I2 = (N - I1)/10
       N = I2
       STR = CHAR(I1 + 48)//STR
    END DO
    RETURN
  END SUBROUTINE NUMTOSTR

END PROGRAM MAIN
