PROGRAM MAIN
  IMPLICIT NONE

  INTEGER, PARAMETER :: LSIZE = 4, NSIZE = LSIZE**2
  INTEGER, DIMENSION(LSIZE,LSIZE) :: ARRAY,PREV_ARR
  INTEGER :: I1,I2,S1,S2,ITEST
  INTEGER :: GAME_COUNT, GAME_SCORE
  CHARACTER(LEN=1), DIMENSION(4) :: INP
  CHARACTER(LEN=1) :: STR

  INP = ['r','l','d','u']
  ARRAY = 0
  CALL RANDOM_INTEGER(1,LSIZE,S1)
  CALL RANDOM_INTEGER(1,LSIZE,S2)
  ARRAY(S1,S2) = 2

  CALL WRITE_ARRAY(ARRAY,6)

  GAME_COUNT = 0
  GAME_SCORE = 0
  DO
     READ*,STR
     PREV_ARR = ARRAY
     !CALL RANDOM_INTEGER4(ITEST)
     SELECT CASE(STR)
     CASE('r') !1
        !! SHIFT RIGHT !!
        DO I1 = 1, LSIZE
           CALL SHIFT_INCREASE(ARRAY(I1,:),GAME_SCORE)
        END DO
     CASE('l') !2
        !! SHIFT LEFT !!
        DO I1 = 1, LSIZE
           CALL SHIFT_DECREASE(ARRAY(I1,:),GAME_SCORE)
        END DO
     CASE('d') !3
        !! SHIFT DOWN !!
        DO I2 = 1, LSIZE
           CALL SHIFT_INCREASE(ARRAY(:,I2),GAME_SCORE)
        END DO
     CASE('u') !4
        !! SHIFT UP !!
        DO I2 = 1, LSIZE
           CALL SHIFT_DECREASE(ARRAY(:,I2),GAME_SCORE)
        END DO
     CASE DEFAULT
        PRINT*,STR,ICHAR(STR)
        STOP 'SOMETHING WRONG'
     END SELECT

     IF (ANY(ARRAY /= PREV_ARR)) THEN
        GAME_COUNT = GAME_COUNT + 1
     ELSE IF (ANY(ARRAY == 0)) THEN
        !PRINT*,'CAN NOT MOVE!'
        CYCLE
     ELSE
        PRINT*,'GAME_COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
        STOP 'GAME OVER'
     END IF

     DO 
        CALL RANDOM_INTEGER(1,LSIZE,S1)
        CALL RANDOM_INTEGER(1,LSIZE,S2)
        IF (ARRAY(S1,S2)==0)THEN
           ARRAY(S1,S2) = 2
           EXIT
        ELSE
           CYCLE
        END IF
     END DO
     PRINT*,'SCORE = ',GAME_SCORE
     CALL WRITE_ARRAY(ARRAY,6)

     !PAUSE
  END DO

CONTAINS

  SUBROUTINE SHIFT_INCREASE(ARR,SCORE)
    IMPLICIT NONE
    INTEGER, DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER, INTENT(INOUT) :: SCORE
    INTEGER :: NUM, I, J, C, TERM
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, 1, -1
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, 1+C, -1
                ARR(J) = ARR(J-C)
             END DO
             ARR(1:C) = 0
          END IF
          !PRINT*,I,C
          !PRINT'(10(I2))',ARR
       END IF
       I = I - 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I-1))THEN
          TERM = ARR(I) * 2
          ARR(I) = TERM
          SCORE = SCORE + TERM
          DO J = I-1, 2, -1
             ARR(J) = ARR(J-1)
          END DO
          ARR(1) = 0
       END IF
       I = I - 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_INCREASE

  SUBROUTINE SHIFT_DECREASE(ARR,SCORE)
    IMPLICIT NONE
    INTEGER, DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER, INTENT(INOUT) :: SCORE
    INTEGER :: NUM, I, J, C, TERM
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, NUM
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, NUM-C
                ARR(J) = ARR(J+C)
             END DO
             ARR(NUM-C+1:NUM) = 0
          END IF
       END IF
       I = I + 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I+1))THEN
          TERM = ARR(I) * 2
          ARR(I) = TERM
          SCORE = SCORE + TERM
          DO J = I+1, NUM - 1
             ARR(J) = ARR(J+1)
          END DO
          ARR(NUM) = 0
       END IF
       I = I + 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_DECREASE

  SUBROUTINE RANDOM_INTEGER(N1,N2,IRAN)
    INTEGER, INTENT(IN) :: N1,N2
    INTEGER, INTENT(OUT) :: IRAN
    REAL(8) :: X
    CALL RANDOM_NUMBER(X)
    IRAN = N1 + FLOOR(X*DBLE(N2 + 1 - N1))
    RETURN
  END SUBROUTINE RANDOM_INTEGER

  SUBROUTINE RANDOM_INTEGER4(IRAN)
    INTEGER, INTENT(OUT) :: IRAN
    REAL(8) :: X
    CALL RANDOM_NUMBER(X)
    IRAN = 1 + INT(4.0D0 * X)
    RETURN
  END SUBROUTINE RANDOM_INTEGER4

  SUBROUTINE WRITE_ARRAY(ARRAY,UNIT)
    IMPLICIT NONE
    INTEGER, DIMENSION(:,:), INTENT(IN) :: ARRAY
    INTEGER, INTENT(IN) :: UNIT
    INTEGER :: I1,I2,NUM
    NUM = SIZE(ARRAY,DIM=1)
    DO I1 = 1, NUM
       DO I2 = 1, NUM
          WRITE(UNIT,'(I4,1X)',ADVANCE='NO')ARRAY(I1,I2)
       END DO;WRITE(UNIT,*)
    END DO
    RETURN
  END SUBROUTINE WRITE_ARRAY

END PROGRAM MAIN
