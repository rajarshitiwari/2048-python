PROGRAM MAIN
  IMPLICIT NONE

  INTEGER(8), PARAMETER :: LSIZE = 8, NSIZE = LSIZE**2
  INTEGER(8), DIMENSION(LSIZE,LSIZE) :: ARRAY,PREV_ARR
  INTEGER(8) :: I1,I2,S1,S2,ITEST,ISAMPLE,NUM_SAMPLE
  INTEGER(8) :: GAME_COUNT, GAME_SCORE
  INTEGER(8) :: AVG_COUNT, AVG_SCORE
  REAL(8) :: T1,T2
  CHARACTER(LEN=1), DIMENSION(4) :: INP
  CHARACTER(LEN=1) :: STR


  NUM_SAMPLE = 100
  OPEN(1,FILE='game_score.dat',ACCESS='APPEND')

  AVG_COUNT = 0
  AVG_SCORE = 0

  LOOP_SAMPLE:  DO ISAMPLE = 1, NUM_SAMPLE
     PRINT*,ISAMPLE
     CALL CPU_TIME(T1)
     CALL RANDOM_SEED()
     INP = ['r','l','d','u']
     ARRAY = 0
     CALL RANDOM_INTEGER(1,LSIZE,S1)
     CALL RANDOM_INTEGER(1,LSIZE,S2)
     ARRAY(S1,S2) = 2

     !CALL WRITE_ARRAY(ARRAY,6)
     GAME_COUNT = 0
     GAME_SCORE = 0
     LOOP_GAME: DO
        PREV_ARR = ARRAY
        CALL RANDOM_INTEGER4(ITEST)
        SELECT CASE(ITEST)
        CASE(1)                 !! SHIFT RIGHT !!
           DO I1 = 1, LSIZE
              CALL SHIFT_INCREASE(ARRAY(I1,:),GAME_SCORE)
           END DO
        CASE(2)                 !! SHIFT LEFT  !!
           DO I1 = 1, LSIZE
              CALL SHIFT_DECREASE(ARRAY(I1,:),GAME_SCORE)
           END DO
        CASE(3)                 !! SHIFT DOWN  !!
           DO I2 = 1, LSIZE
              CALL SHIFT_INCREASE(ARRAY(:,I2),GAME_SCORE)
           END DO
        CASE(4)                 !! SHIFT UP    !!
           DO I2 = 1, LSIZE
              CALL SHIFT_DECREASE(ARRAY(:,I2),GAME_SCORE)
           END DO
        CASE DEFAULT
           PRINT*,STR,ICHAR(STR)
           PRINT*,'SOMETHING WRONG'
        END SELECT

        IF (ANY(ARRAY /= PREV_ARR)) THEN
           GAME_COUNT = GAME_COUNT + 1
        ELSE IF (ANY(ARRAY == 0)) THEN !! CAN NOT MOVE !!
           CYCLE
        ELSE                    !! EXIT THE GAME !!
           EXIT
        END IF

        DO WHILE (ANY(ARRAY == 0)) 
           CALL RANDOM_INTEGER(1,LSIZE,S1)
           CALL RANDOM_INTEGER(1,LSIZE,S2)
           IF (ARRAY(S1,S2)==0)THEN
              ARRAY(S1,S2) = 2
              EXIT
           ELSE
              CYCLE
           END IF
        END DO
        !PRINT*,'GAME_COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
        !WRITE(1,*)GAME_COUNT,GAME_SCORE
        !CALL WRITE_ARRAY(ARRAY,6)
        !CALL SYSTEM('sleep 0.5')
        !PAUSE
     END DO LOOP_GAME

     CALL CPU_TIME(T2)
     AVG_COUNT = AVG_COUNT + GAME_COUNT
     AVG_SCORE = AVG_SCORE + GAME_SCORE

     WRITE(1,'(I10,2(ES23.14,1X))')ISAMPLE,DBLE(AVG_COUNT)/DBLE(ISAMPLE),DBLE(AVG_SCORE)/DBLE(ISAMPLE)
     PRINT*,'TIME = ',T2 - T1
  END DO LOOP_SAMPLE
  PRINT*,'AVERAGE COUNT = ',AVG_COUNT/NUM_SAMPLE
  PRINT*,'AVERAGE SCORE = ',AVG_SCORE/NUM_SAMPLE
  WRITE(1,*)

CONTAINS

  SUBROUTINE SHIFT_INCREASE(ARR,SCORE)
    IMPLICIT NONE
    INTEGER, DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER, INTENT(INOUT) :: SCORE
    INTEGER :: NUM, I, J, C, TERM
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, 1, -1
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, 1+C, -1
                ARR(J) = ARR(J-C)
             END DO
             ARR(1:C) = 0
          END IF
          !PRINT*,I,C
          !PRINT'(10(I2))',ARR
       END IF
       I = I - 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = NUM
    DO WHILE ( I >= 2 )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I-1))THEN
          TERM = ARR(I) * 2
          ARR(I) = TERM
          SCORE = SCORE + TERM
          DO J = I-1, 2, -1
             ARR(J) = ARR(J-1)
          END DO
          ARR(1) = 0
       END IF
       I = I - 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_INCREASE

  SUBROUTINE SHIFT_DECREASE(ARR,SCORE)
    IMPLICIT NONE
    INTEGER, DIMENSION(:), INTENT(INOUT) :: ARR
    INTEGER, INTENT(INOUT) :: SCORE
    INTEGER :: NUM, I, J, C, TERM
    NUM = SIZE(ARR)
!!! SHIFT ALL ZEROS TO RIGHT
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) THEN
          C = 0
          DO J = I, NUM
             IF (ARR(J) == 0)THEN
                C = C + 1
             ELSE
                EXIT
             END IF
          END DO
          IF (C /= 0)THEN
             DO J = I, NUM-C
                ARR(J) = ARR(J+C)
             END DO
             ARR(NUM-C+1:NUM) = 0
          END IF
          !PRINT*,I,C
          !PRINT'(10(I2))',ARR
       END IF
       I = I + 1
    END DO
!!! ADD CONSECUTIVE NUMBERS !!!
    C = 0
    I = 1
    DO WHILE ( I < NUM )
       IF (ARR(I) == 0) EXIT
       IF (ARR(I) == ARR(I+1))THEN
          TERM = ARR(I) * 2
          ARR(I) = TERM
          SCORE = SCORE + TERM
          DO J = I+1, NUM - 1
             ARR(J) = ARR(J+1)
          END DO
          ARR(NUM) = 0
       END IF
       I = I + 1
    END DO
    !PRINT'(10(I2))',ARR
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    RETURN
  END SUBROUTINE SHIFT_DECREASE

  SUBROUTINE RANDOM_INTEGER(N1,N2,IRAN)
    INTEGER, INTENT(IN) :: N1,N2
    INTEGER, INTENT(OUT) :: IRAN
    REAL(8) :: X
    CALL RANDOM_NUMBER(X)
    IRAN = N1 + FLOOR(X*DBLE(N2 + 1 - N1))
    RETURN
  END SUBROUTINE RANDOM_INTEGER

  SUBROUTINE RANDOM_INTEGER4(IRAN)
    INTEGER, INTENT(OUT) :: IRAN
    REAL(8) :: X
    CALL RANDOM_NUMBER(X)
    IRAN = 1 + INT(4.0D0 * X)
    RETURN
  END SUBROUTINE RANDOM_INTEGER4

  SUBROUTINE WRITE_ARRAY(ARRAY,UNIT)
    IMPLICIT NONE
    INTEGER, DIMENSION(:,:), INTENT(IN) :: ARRAY
    INTEGER, INTENT(IN) :: UNIT
    INTEGER :: I1,I2,NUM

    NUM = SIZE(ARRAY,DIM=1)

    DO I1 = 1, NUM
       DO I2 = 1, NUM
          WRITE(UNIT,'(I4,1X)',ADVANCE='NO')ARRAY(I1,I2)
       END DO;WRITE(UNIT,*)
    END DO
    RETURN
  END SUBROUTINE WRITE_ARRAY

END PROGRAM MAIN
