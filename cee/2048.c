#include <time.h>
#include <stdlib.h>
#include <ncurses.h>

int shift_increase(int arr[], int num);
int shift_decrease(int arr[], int num);

int main()
{
  int lsize = 4;
  int curr_arr[lsize][lsize];
  int prev_arr[lsize][lsize];
  int arr1d[lsize];
  int game_count, game_score;
  int game_end, idiff,izero;
  int r1,r2;
  int i1,i2,i;
  int str;


  initscr();
  //start_color();
  cbreak();

  keypad(stdscr, TRUE);
  noecho();
  //init_pair(1, COLOR_CYAN, COLOR_BLACK);
  
  for (i1 = 0; i1 < lsize; i1++)
    {
      for (i2 = 0; i2 < lsize; i2++)
	{
	  curr_arr[i1][i2] = 0;
	}
    }
  
  r1 = rand() % 4;
  r2 = rand() % 4;
  
  curr_arr[r1][r2] = 2;
  

  game_count = 0;
  game_score = 0;
  
  game_end = 0;

  while((str = getch()) != 'q')
    {
      for (i1 = 0; i1 < lsize; i1++)
	{
	  for (i2 = 0; i2 < lsize; i2++)
	    prev_arr[i1][i2] = curr_arr[i1][i2];
	}
      
      switch (str)
	{
	case KEY_RIGHT:		/* SHIFT RIGHT */
	  for (i1 = 1; i1 < lsize; i1++)
	    {
	      for (i = 1; i < lsize; i++)
		arr1d[i] = curr_arr[i1][i];
	      game_score += shift_increase(arr1d,game_score);
	    }
	case KEY_LEFT:		/* SHIFT LEFT */
	  for (i1 = 1; i1 < lsize; i1++)
	    {
	      for (i = 1; i < lsize; i++)
		arr1d[i] = curr_arr[i1][i];
	      game_score += shift_decrease(arr1d,game_score);
	    }
	case KEY_DOWN:		/* SHIFT DOWN */
	  for (i2 = 1; i2 < lsize; i2++)
	    {
	      for (i = 1; i < lsize; i++)
		arr1d[i] = curr_arr[i][i2];
	      game_score += shift_increase(arr1d,game_score);
	    }
	case KEY_UP:		/* SHIFT UP */
	  for (i2 = 1; i2 < lsize; i2++)
	    {
	      for (i = 1; i < lsize; i++)
		arr1d[i] = curr_arr[i][i2];
	      game_score += shift_decrease(arr1d,game_score);
	    }
	}
      
      idiff = 0;
      izero = 0;
      for (i1 = 0; i1 < lsize; i1++)
	{
	  for (i2 = 0; i2 < lsize; i2++)
	    {
	      if (prev_arr[i1][i2] != curr_arr[i1][i2])
		idiff +=1;
	      if (curr_arr[i1][i2] == 0)
		izero +=1;
	    }
	}
      
      for (i1 = 0; i1 < lsize; i1++)
	{
	  for (i2 = 0; i2 < lsize; i2++)
	    {
	      printw("%d ",curr_arr[i1][i2]);
	    }
	  printw("\n");
	}
      if (idiff != 0)
	game_count += 1;
      else if (izero > 0)
	/* !PRINT*,'CAN NOT MOVE!' */
	continue;
      else
	{
	  //PRINT*,'GAME_COUNT = ',GAME_COUNT,'SCORE = ',GAME_SCORE
	  // 'GAME OVER'
	  game_end = 1;
	  break;
	}
      //return game_score;
      
      while ( game_end != 1 )
	{ 
	  r1 = rand() % lsize;
	  r2 = rand() % lsize;
	  if (curr_arr[r1][r2] == 0)
	    {
	      curr_arr[r1][r2] = 2;
	      break;
	    }
	}
      //PRINT*,'SCORE = ',GAME_SCORE
      //CALL WRITE_ARRAY(CURR_ARR,6)
      //!PAUSE
    }
  endwin();
  return 0;
}



int shift_increase(int arr[], int num)
{
  int i, j, c, score, term;
  c = 0;			/* SHIFT ALL ZEROS TO RIGHT */
  i = num-1;
  while ( i > 0 )
    {
      if (arr[i] == 0)
	{
          c = 0;
          for ( j = i; j>=0; j--)
	    {
	      if (arr[j] == 0)
		c += 1;
	      else
                break;
	    }
          if (c != 0)
	    {
	      for ( j = i; j>=c; j--)
                arr[j] = arr[j-c];
	      for ( j = 0; j<c; j++)
		arr[j] = 0;
	    }
	}
      i -= 1;
    }
  c = 0;			/* add consecutive numbers */
  i = num-1;
  score = 0;
  while ( i >= 1 )
    {
      if (arr[i] == 0)
  	break;
      if (arr[i] == arr[i-1])
  	{
          term = arr[i] * 2;
	  arr[i] = term;
	  score += term;
          for ( j = i-1; j >= 1; j--)
  	    arr[j] = arr[j-1];
  	  arr[0] = 0;
  	}
      i -= 1;
    }
  return score;
}

int shift_decrease(int arr[], int num)
{
  int i, j, c, score, term;
  c = 0;			/* shift all zeros to right  */
  i = 0;
  while ( i < num - 1 )
    {
      if (arr[i] == 0)
	{
	  c = 0;
	  for ( j = i; j < num; j++)
	    {
	      if (arr[j] == 0)
		c += 1;
	      else
		break;
	    }
	  if (c != 0)
	    {
	      for ( j = i; j < num-c; j++)
		arr[j] = arr[j+c];
	      for (j = num-c; j<num; j++)
		arr[j] = 0;
	    }
	}
      i += 1;
    }
  c = 0;			/* add consecutive numbers */
  i = 0;
  score = 0;
  while ( i < num-1 )
    {
      if (arr[i] == 0)
	break;
      if (arr[i] == arr[i+1])
	{
	  term = arr[i] * 2;
	  arr[i] = term;
	  score += term;
          for ( j = i+1; j < num - 1; j++)
	    arr[j] = arr[j+1];
	  arr[num-1] = 0;
	}
      i += 1;
    }
  return score;
}

























  /* initscr();			/\* Start curses mode      *\/ */
  /* raw(); 			/\* Line buffering disable *\/ */
  /* keypad(stdscr, TRUE); 	/\* We get F1, F2 etc ...  *\/ */
  /* noecho();			/\* Don't echo() while we do getch *\/ */

  /* printw("Type any character to see it in bold\n"); */

  /* ch = getch();			/\* raw input *\/ */
  /* if (ch == KEY_F(2)) */
  /*   printw("F2 key pressed"); */
  /* else */
  /*   { */
  /*     printw("The pressed key is "); */
  /*     attron(A_BOLD); */
  /*     attron(A_UNDERLINE); */
  /*     attron(COLOR_PAIR(1)); */
  /*     printw("%c", ch); */
  /*     attroff(A_BOLD); */
  /*     attroff(A_UNDERLINE); */
  /*     attroff(COLOR_PAIR(1)); */
  /*   } */

  /* //  printw("Hello World !!!");	/\* Print Hello World *\/ */
  /* refresh();			/\* Print it on to the real screen *\/ */
  /* getch();			/\* Wait for user input *\/ */
  /* endwin();			/\* End curses mode *\/ */
